resource "aws_s3_bucket" "bucket" {
  provider = aws.region-master
  bucket = "my-tf-test-bucket-8648773298"
  acl    = "private"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}