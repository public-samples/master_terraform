resource "aws_api_gateway_rest_api" "api" {
  provider    = aws.region-master
  name        = "ServerlessExample"
  description = "Serverless example built with Terraform"
}

resource "aws_api_gateway_resource" "resource" {
  provider    = aws.region-master
  rest_api_id = aws_api_gateway_rest_api.api.id
  parent_id   = aws_api_gateway_rest_api.api.root_resource_id
  path_part   = "mydemoresource"
}

resource "aws_api_gateway_method" "method" {
  provider      = aws.region-master
  rest_api_id   = aws_api_gateway_rest_api.api.id
  resource_id   = aws_api_gateway_resource.resource.id
  http_method   = "GET"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "integration" {
  provider                = aws.region-master
  rest_api_id             = aws_api_gateway_rest_api.api.id
  resource_id             = aws_api_gateway_resource.resource.id
  http_method             = aws_api_gateway_method.method.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.simple_lambda.invoke_arn
}
