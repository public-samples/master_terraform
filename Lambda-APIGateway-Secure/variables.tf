variable "profile" {
  type    = "string"
  default = "default"
}

variable "region-master" {
  type    = "string"
  default = "eu-west-1"
}

variable "environment" {
  type    = "string"
  default = "dev"
}

variable "project" {
  type    = string
  default = "moon"
}

variable "runtime" {
  type    = "string"
  default = "nodejs12.x"
}

variable "accountId" {
  type    = "string"
  default = "XXXXXXX"
}