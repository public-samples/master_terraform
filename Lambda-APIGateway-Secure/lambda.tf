data "archive_file" "lambda_zip" {
  type        = "zip"
  source_file = "sample.js"
  output_path = "lambda_function.zip"
}

resource "aws_lambda_function" "simple_lambda" {
  provider         = aws.region-master
  filename         = "lambda_function.zip"
  function_name    = "${var.environment}-${var.project}"
  role             = aws_iam_role.lambda_role.arn
  handler          = "sample.handler"
  source_code_hash = "${data.archive_file.lambda_zip.output_base64sha256}"
  runtime          = var.runtime
}

resource "aws_iam_role" "lambda_role" {
  provider           = aws.region-master
  name               = "${var.environment}_${var.project}"
  assume_role_policy = "${file("iam_role_lambda.json")}"
}

resource "aws_lambda_permission" "apigw_lambda" {
  provider      = aws.region-master
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.simple_lambda.function_name
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = "arn:aws:execute-api:${var.region-master}:${var.accountId}:${aws_api_gateway_rest_api.api.id}/*/${aws_api_gateway_method.method.http_method}${aws_api_gateway_resource.resource.path}"
}