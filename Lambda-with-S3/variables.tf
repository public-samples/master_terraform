# Generic Variables

variable "profile" {
  type    = string
  default = "default"
}

variable "region-master" {
  type    = string
  default = "eu-west-1"
}

# API Variables

variable "API_name" {
  type    = string
  default = "API"
}

variable "resource_path" {
  type    = string
  default = "resource"
}

# S3 Variables

variable "bucket_name" {
  type    = string
  default = "764786538433rfsd8887689990"
}