
resource "aws_s3_bucket" "bucket" {
  bucket   = var.bucket_name
  acl      = "private"
  policy = data.aws_iam_policy_document.policy.json
}

data "aws_iam_policy_document" "policy" {
  statement {
    actions = [
      "s3:GetObject"
    ]
    principals {
      identifiers = ["*"]
      type = "AWS"
    }
    resources = [
      "arn:aws:s3:::${var.bucket_name}/*"
    ]
  }
}
resource "aws_s3_bucket_object" "object" {
  bucket       = aws_s3_bucket.bucket.bucket
  key          = "${var.object}"
  source       = "./Bucket_1/contents/${var.object}"
  content_type = "text/html"
}

