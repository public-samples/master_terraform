variable "bucket_name" {
    type = string
    description = "Name for Bucket"
}

variable "object" {
    type = string
    default = "test.jpg"
    description = "(optional) describe your variable"
}