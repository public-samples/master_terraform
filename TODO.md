## TODO

- multiple EC2 deployment with LB and ASG
- Lambda that interacts with DynamoDB
- SQS
- S3 bucket - lambda - S3 Bucket (perhaps create an image thumbnail)

## Complete

- DynamoDB-Deployment
- EC2-deployment-with-VPC
- Gitlab-runner-single
- LA-multi-AZ-Jenkins_deployment
- Lambda-APIGateway-Simple
- S3-Simple
- S3-Static-website
- Simple-EC2-deployment
- EC2-Deployment-with-Modules

## WIP

- Lambda-APIGateway-Secure
    - Need to check the best practice in Lambda-APIGateway-Secure
- S3-website-with-Lambda
    - issue with putting the invoke-url for the lambda function into the index.html - solutions could include local-exec or templatefile.
- Lambda-with-S3
    - Stopped and started - issue with the policies for the S3 buckets