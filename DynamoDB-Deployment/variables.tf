variable "profile" {
  type    = string
  default = "default"
}

variable "region-master" {
  type    = string
  default = "eu-west-1"
}

variable "region-replica" {
  type    = string
  default = "eu-west-2"
}

variable "instance-type" {
  type    = string
  default = "t2.micro"
}