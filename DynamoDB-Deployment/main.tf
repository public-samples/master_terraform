resource "aws_dynamodb_table" "eu-west-1" {
  provider = aws.region-master

  hash_key         = "myAttribute"
  name             = "myTable"
  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"
  read_capacity    = 1
  write_capacity   = 1

  attribute {
    name = "myAttribute"
    type = "S"
  }
}

resource "aws_dynamodb_table" "eu-west-2" {
  provider = aws.region-replica

  hash_key         = "myAttribute"
  name             = "myTable"
  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"
  read_capacity    = 1
  write_capacity   = 1

  attribute {
    name = "myAttribute"
    type = "S"
  }
}

resource "aws_dynamodb_global_table" "myTable" {
  depends_on = [
    aws_dynamodb_table.eu-west-1,
    aws_dynamodb_table.eu-west-2,
  ]
  provider = aws.region-master

  name = "myTable"

  replica {
    region_name = "eu-west-1"
  }

  replica {
    region_name = "eu-west-2"
  }
}