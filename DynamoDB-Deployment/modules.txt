module "dynamodb-table-eu-west-1" {
  source = "./dynamodb-table"
}

module "dynamodb-table-eu-west-2" {
  source = "./dynamodb-table"

  providers = {
    aws = "replica"
  }
}

resource "aws_dynamodb_global_table" "global-user-tables" {
  depends_on = [
    "module.dynamodb-table-eu-west-1",
  "module.dynamodb-table-eu-west-2"]

  name = "customers"

  replica {
    region_name = "eu-west-1"
  }

  replica {
    region_name = "eu-west-2"
  }

}