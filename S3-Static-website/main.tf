resource "aws_s3_bucket" "website_bucket" {
  provider = aws.region-master
  bucket   = "s3-website-test344343.hashicorp.com"
  acl      = "public-read"
  policy   = data.aws_iam_policy_document.bucket_policy.json

  website {
    index_document = "index.html"
    # error_document = "error.html"
  }
}

data "aws_iam_policy_document" "bucket_policy" {
  provider = aws.region-master
  statement {
    sid = "AllowReadFromAll"

    actions = [
      "s3:GetObject",
    ]

    resources = [
      "arn:aws:s3:::s3-website-test344343.hashicorp.com/*",
    ]

    principals {
      type        = "*"
      identifiers = ["*"]
    }
  }
}


resource "aws_s3_bucket_object" "index" {
  provider     = aws.region-master
  bucket       = aws_s3_bucket.website_bucket.bucket
  key          = "index.html"
  source       = "./index.html"
  content_type = "text/html"
}

# resource "aws_s3_bucket_object" "error" {
#   bucket       = local.bucket_name
#   key          = "error.html"
#   source       = "initial_files/error.html"
#   content_type = "text/html"
# }
