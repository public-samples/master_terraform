data "aws_availability_zones" "azs" {
  provider = aws.region-master
  state    = "available"
}

resource "aws_vpc" "simple_vpc" {
  provider             = aws.region-master
  cidr_block           = "10.0.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "simple_vpc"
  }
}

resource "aws_internet_gateway" "igw" {
  provider = aws.region-master
  vpc_id   = aws_vpc.simple_vpc.id
}

resource "aws_subnet" "simple_subnet_1" {
  provider          = aws.region-master
  availability_zone = element(data.aws_availability_zones.azs.names, 0)
  vpc_id            = aws_vpc.simple_vpc.id
  cidr_block        = "10.0.1.0/24"
}

resource "aws_route_table" "internet_route" {
  provider = aws.region-master
  vpc_id   = aws_vpc.simple_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
  tags = {
    Name = "Simple VPC Route Table"
  }
}

resource "aws_main_route_table_association" "simple_route_table_association" {
  provider       = aws.region-master
  vpc_id         = aws_vpc.simple_vpc.id
  route_table_id = aws_route_table.internet_route.id
}
