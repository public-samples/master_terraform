data "http" "my_public_ip" {
  url = "https://ifconfig.me"
  request_headers = {
    Accept = "application/json"
  }
}

locals {
  my_ip = data.http.my_public_ip.body
}

resource "aws_security_group" "simple_sg" {
  provider    = aws.region-master
  name        = "simple_sg"
  description = "Allow SSH access from host machine and access to internet"
  vpc_id      = aws_vpc.simple_vpc.id
  ingress {
    description = "Allow 22 from host machine"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${local.my_ip}/32"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}