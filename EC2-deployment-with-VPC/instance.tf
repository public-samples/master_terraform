data "aws_ssm_parameter" "linuxAmi" {
  provider = aws.region-master
  name     = "/aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2"
}

resource "aws_key_pair" "master-key" {
  provider   = aws.region-master
  key_name   = "simple_ec2"
  public_key = file("~/.ssh/id_rsa.pub")
}

resource "aws_instance" "simple_ec2" {
  provider                    = aws.region-master
  ami                         = data.aws_ssm_parameter.linuxAmi.value
  instance_type               = var.instance-type
  key_name                    = aws_key_pair.master-key.key_name
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.simple_sg.id]
  subnet_id                   = aws_subnet.simple_subnet_1.id

  tags = {
    Name = "simple_ec2"
  }

  depends_on = [aws_main_route_table_association.simple_route_table_association]

}
