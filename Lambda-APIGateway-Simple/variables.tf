variable "profile" {
  type    = "string"
  default = "default"
}

variable "region-master" {
  type    = "string"
  default = "eu-west-1"
}

variable "runtime" {
  type    = "string"
  default = "nodejs12.x"
}
# Need to check if this is best practice, should the accountId be kept secure?
variable "accountId" {
  type    = "string"
  default = "271447827183"
}