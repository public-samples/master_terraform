data "archive_file" "lambda_zip" {
  type        = "zip"
  source_file = "sample.js"
  output_path = "lambda_function.zip"
}

resource "aws_lambda_function" "simple_lambda" {
  provider         = aws.region-master
  filename         = "lambda_function.zip"
  function_name    = "simple_lambda"
  role             = aws_iam_role.lambda_role.arn
  handler          = "sample.handler"
  source_code_hash = "${data.archive_file.lambda_zip.output_base64sha256}"
  runtime          = var.runtime
}

resource "aws_iam_role" "lambda_role" {
  provider           = aws.region-master
  name               = "lambda_role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_lambda_permission" "apigw_lambda" {
  provider      = aws.region-master
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.simple_lambda.function_name
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = "arn:aws:execute-api:${var.region-master}:${var.accountId}:${aws_api_gateway_rest_api.MyDemoAPI.id}/*/${aws_api_gateway_method.MyDemoMethod.http_method}${aws_api_gateway_resource.MyDemoResource.path}"
}