
resource "aws_s3_bucket" "website_bucket" {
  bucket   = var.bucket_name
  acl      = "public-read"
  # policy = file("./S3/policy.json")
  policy = data.aws_iam_policy_document.website_policy.json

  website {
    # index_document = templatefile("./S3/index.tpl", {
    #   url = var.url
    # })
    index_document = "index.html"
  }
}

data "aws_iam_policy_document" "website_policy" {
  statement {
    actions = [
      "s3:GetObject"
    ]
    principals {
      identifiers = ["*"]
      type = "AWS"
    }
    resources = [
      "arn:aws:s3:::${var.bucket_name}/*"
    ]
  }
}
resource "aws_s3_bucket_object" "index" {
  bucket       = aws_s3_bucket.website_bucket.bucket
  key          = "index.html"
  source       = "./S3/index.html"
  content_type = "text/html"
}

