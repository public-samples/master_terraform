variable "profile" {
  type    = string
  default = "default"
}

variable "region-master" {
  type    = string
  default = "eu-west-1"
}

variable "bucket_name" {
  type    = string
  default = "764786538438887689990"
}