variable "region-master" {
    type = string
    description = "(optional) describe your variable"
}

variable "environment" {
  type    = string
  default = "dev"
}

variable "project" {
  type    = string
  default = "moon"
}

variable "runtime" {
  type    = string
  default = "nodejs12.x"
}

variable "accountId" {
  type    = string
  default = "XXXXXXXXXXXX"
}

variable "aws_api_gateway_rest_api_id" {
  type = string
  description = "(optional) describe your variable"
}

variable "aws_api_gateway_method_http_method" {
  type = string
  description = "(optional) describe your variable"
}

variable "aws_api_gateway_resource_path" {
  type = string
  description = "(optional) describe your variable"
}