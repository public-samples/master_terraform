provider "aws" {
  profile = var.profile
  region  = var.region-master
}

module "API_Gateway" {
  source                       = "./API_Gateway"
  hello_name_lambda_invoke_arn = module.Lambda.hello_name_lambda_invoke_arn
}

module "Lambda" {
  source                             = "./Lambda"
  region-master                      = var.region-master
  aws_api_gateway_rest_api_id        = module.API_Gateway.aws_api_gateway_rest_api_id
  aws_api_gateway_method_http_method = module.API_Gateway.aws_api_gateway_method_http_method
  aws_api_gateway_resource_path      = module.API_Gateway.aws_api_gateway_resource_path
}

module "S3" {
  source      = "./S3"
  bucket_name = var.bucket_name
  depends_on  = [module.API_Gateway]
  url         = module.API_Gateway.url
}
