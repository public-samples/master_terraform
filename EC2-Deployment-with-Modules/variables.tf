variable "region-master" {
  type    = "string"
  default = "eu-west-1"
}

variable "profile" {
  type    = "string"
  default = "default"
}