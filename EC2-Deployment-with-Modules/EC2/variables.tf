variable "instance-type" {
  type    = "string"
  default = "t2.micro"
}

variable "pem-key" {
    type = string
    default = "simple_ec2"
}

variable "public_subnet_A" {
  type = string
  description = "(optional) describe your variable"
}
variable "vpc_security_group_ids" {
  type = string
  description = "(optional) describe your variable"
}

variable "associate_public_ip_address" {
  type = string
  description = "(optional) describe your variable"
}