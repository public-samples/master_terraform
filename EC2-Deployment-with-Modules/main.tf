provider "aws" {
  region = var.region-master
}

module "aws_vpc" {
  source = "./VPC"
}

module "aws_simple_sg" {
  source = "./SecurityGroup"
  name   = "simple_sg"
  vpc_id = module.aws_vpc.vpc_id
}

module "webserver_node" {
  source                      = "./EC2"
  public_subnet_A             = module.aws_vpc.public_subnetA_id
  vpc_security_group_ids      = module.aws_simple_sg.aws_sg
  associate_public_ip_address = true
}
